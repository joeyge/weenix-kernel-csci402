/******************************************************************************/
/* Important Spring 2022 CSCI 402 usage information:                          */
/*                                                                            */
/* This fils is part of CSCI 402 kernel programming assignments at USC.       */
/*         53616c7465645f5fd1e93dbf35cbffa3aef28f8c01d8cf2ffc51ef62b26a       */
/*         f9bda5a68e5ed8c972b17bab0f42e24b19daa7bd408305b1f7bd6c7208c1       */
/*         0e36230e913039b3046dd5fd0ba706a624d33dbaa4d6aab02c82fe09f561       */
/*         01b0fd977b0051f0b0ce0c69f7db857b1b5e007be2db6d42894bf93de848       */
/*         806d9152bd5715e9                                                   */
/* Please understand that you are NOT permitted to distribute or publically   */
/*         display a copy of this file (or ANY PART of it) for any reason.    */
/* If anyone (including your prospective employer) asks you to post the code, */
/*         you must inform them that you do NOT have permissions to do so.    */
/* You are also NOT permitted to remove or alter this comment block.          */
/* If this comment block is removed or altered in a submitted file, 20 points */
/*         will be deducted.                                                  */
/******************************************************************************/

#include "types.h"
#include "globals.h"
#include "errno.h"

#include "util/debug.h"
#include "util/string.h"

#include "proc/proc.h"
#include "proc/kthread.h"

#include "mm/mm.h"
#include "mm/mman.h"
#include "mm/page.h"
#include "mm/pframe.h"
#include "mm/mmobj.h"
#include "mm/pagetable.h"
#include "mm/tlb.h"

#include "fs/file.h"
#include "fs/vnode.h"

#include "vm/shadow.h"
#include "vm/vmmap.h"

#include "api/exec.h"

#include "main/interrupt.h"

/* Pushes the appropriate things onto the kernel stack of a newly forked thread
 * so that it can begin execution in userland_entry.
 * regs: registers the new thread should have on execution
 * kstack: location of the new thread's kernel stack
 * Returns the new stack pointer on success. */
static uint32_t
fork_setup_stack(const regs_t *regs, void *kstack)
{
        /* Pointer argument and dummy return address, and userland dummy return
         * address */
        uint32_t esp = ((uint32_t) kstack) + DEFAULT_STACK_SIZE - (sizeof(regs_t) + 12);
        *(void **)(esp + 4) = (void *)(esp + 8); /* Set the argument to point to location of struct on stack */
        memcpy((void *)(esp + 8), regs, sizeof(regs_t)); /* Copy over struct */
        return esp;
}


/*
 * The implementation of fork(2). Once this works,
 * you're practically home free. This is what the
 * entirety of Weenix has been leading up to.
 * Go forth and conquer.
 */
int
do_fork(struct regs *regs)
{
    vmarea_t *vma, *clone_vma;
    pframe_t *pf;
    mmobj_t *to_delete, *new_shadowed;

// NOT_YET_IMPLEMENTED("VM: do_fork");
// return 0;
    KASSERT(regs != NULL); /* the function argument must be non-NULL */
    dbg(DBG_PRINT, "(GRADING3A 7.a)\n");
    KASSERT(curproc != NULL); /* the parent process, which is curproc, must be non-NULL */
    dbg(DBG_PRINT, "(GRADING3A 7.a)\n");
    KASSERT(curproc->p_state == PROC_RUNNING); /* the parent process must be in the running state and not in the zombie state */
    dbg(DBG_PRINT, "(GRADING3A 7.a)\n");

    // 1. newproc & 7. Set the child's working directory
    proc_t *newproc = proc_create("child process");
    KASSERT(newproc->p_state == PROC_RUNNING); /* new child process starts in the running state */
    dbg(DBG_PRINT, "(GRADING3A 7.a)\n");
    KASSERT(newproc->p_pagedir != NULL); /* new child process must have a valid page table */
    dbg(DBG_PRINT, "(GRADING3A 7.a)\n");

    // 2. copy vmmap & refcount++
    // 3. shadow obj in private page，attention: refcount++
    newproc->p_vmmap = vmmap_clone(curproc->p_vmmap);
    newproc->p_vmmap->vmm_proc = newproc;

    vmarea_t *parent_vmarea = NULL;
    vmarea_t *child_vmarea = NULL;
    mmobj_t *bottomobj = NULL;
    list_iterate_begin(&(curproc->p_vmmap->vmm_list), parent_vmarea, vmarea_t, vma_plink) {
        dbg(DBG_PRINT, "(GRADING3A)\n");
        child_vmarea = vmmap_lookup(newproc->p_vmmap, parent_vmarea->vma_start);
        if(parent_vmarea->vma_flags & MAP_PRIVATE){
            dbg(DBG_PRINT, "(GRADING3A)\n");
            //point the vmarea_t at the new shadow object
            mmobj_t *child_s = shadow_create();
            mmobj_t *parent_s = shadow_create();

            child_s->mmo_shadowed = parent_vmarea->vma_obj;
            parent_s->mmo_shadowed = parent_vmarea->vma_obj;
            parent_vmarea->vma_obj->mmo_ops->ref(parent_vmarea->vma_obj);

            //point to the bottommost object
            bottomobj = mmobj_bottom_obj(parent_vmarea->vma_obj);
            child_s->mmo_un.mmo_bottom_obj = bottomobj;
            parent_s->mmo_un.mmo_bottom_obj = bottomobj;
            bottomobj->mmo_ops->ref(bottomobj);
            bottomobj->mmo_ops->ref(bottomobj);

            list_insert_head(&bottomobj->mmo_un.mmo_vmas, &child_vmarea->vma_olink);

            parent_vmarea->vma_obj = parent_s;
            child_vmarea->vma_obj = child_s;
        }
        else{
            dbg(DBG_PRINT, "(GRADING3D 1)\n");
            child_vmarea->vma_obj = parent_vmarea->vma_obj;
            parent_vmarea->vma_obj->mmo_ops->ref(parent_vmarea->vma_obj);
        }
    } list_iterate_end();

    // 4. Unmap the user land page table entries and flush the TLB, make R/O
    pt_unmap_range(curproc->p_pagedir, USER_MEM_LOW, USER_MEM_HIGH);
    tlb_flush_all();

    // 6. Copy the file descriptor table of the parent into the child
    for(int i = 0; i < NFILES; i++){
        dbg(DBG_PRINT, "(GRADING3A)\n");
        newproc->p_files[i] = curproc->p_files[i];
        if(newproc->p_files[i]!=NULL){
            dbg(DBG_PRINT, "(GRADING3A)\n");
            fref(newproc->p_files[i]);
        }
    }

    // 8. kthread clone
    kthread_t *newthr = kthread_clone(curthr);
    KASSERT(newthr->kt_kstack != NULL); /* thread in the new child process must have a valid kernel stack */
    dbg(DBG_PRINT, "(GRADING3A 7.a)\n");

    // 5. Set up the new process thread context (kt_ctx)
    regs->r_eax = 0;
    newthr->kt_ctx.c_pdptr = newproc->p_pagedir;
    newthr->kt_ctx.c_eip = (uint32_t)userland_entry;
    newthr->kt_ctx.c_esp = fork_setup_stack(regs, newthr->kt_kstack);
    newthr->kt_ctx.c_kstack = (uintptr_t)newthr->kt_kstack;
    newthr->kt_ctx.c_kstacksz = DEFAULT_STACK_SIZE;
    newthr->kt_proc = newproc;
    list_insert_tail(&newproc->p_threads, &newthr->kt_plink);

    // 9.Set any other fields in the new process which need to be set.
    // TODO: p_brk, p_start_brk, after do_brk()
    newproc->p_brk = curproc->p_brk;
    newproc->p_start_brk = curproc->p_start_brk;

    //10. Make the new thread runnable
    sched_make_runnable(newthr);
    dbg(DBG_PRINT, "(GRADING3A)\n");
    return newproc->p_pid;
}
