/******************************************************************************/
/* Important Spring 2022 CSCI 402 usage information:                          */
/*                                                                            */
/* This fils is part of CSCI 402 kernel programming assignments at USC.       */
/*         53616c7465645f5fd1e93dbf35cbffa3aef28f8c01d8cf2ffc51ef62b26a       */
/*         f9bda5a68e5ed8c972b17bab0f42e24b19daa7bd408305b1f7bd6c7208c1       */
/*         0e36230e913039b3046dd5fd0ba706a624d33dbaa4d6aab02c82fe09f561       */
/*         01b0fd977b0051f0b0ce0c69f7db857b1b5e007be2db6d42894bf93de848       */
/*         806d9152bd5715e9                                                   */
/* Please understand that you are NOT permitted to distribute or publically   */
/*         display a copy of this file (or ANY PART of it) for any reason.    */
/* If anyone (including your prospective employer) asks you to post the code, */
/*         you must inform them that you do NOT have permissions to do so.    */
/* You are also NOT permitted to remove or alter this comment block.          */
/* If this comment block is removed or altered in a submitted file, 20 points */
/*         will be deducted.                                                  */
/******************************************************************************/

#include "types.h"
#include "globals.h"
#include "kernel.h"
#include "errno.h"

#include "util/gdb.h"
#include "util/init.h"
#include "util/debug.h"
#include "util/string.h"
#include "util/printf.h"

#include "mm/mm.h"
#include "mm/page.h"
#include "mm/pagetable.h"
#include "mm/pframe.h"

#include "vm/vmmap.h"
#include "vm/shadowd.h"
#include "vm/shadow.h"
#include "vm/anon.h"

#include "main/acpi.h"
#include "main/apic.h"
#include "main/interrupt.h"
#include "main/gdt.h"

#include "proc/sched.h"
#include "proc/proc.h"
#include "proc/kthread.h"

#include "drivers/dev.h"
#include "drivers/blockdev.h"
#include "drivers/disk/ata.h"
#include "drivers/tty/virtterm.h"
#include "drivers/pci.h"

#include "api/exec.h"
#include "api/syscall.h"

#include "fs/vfs.h"
#include "fs/vnode.h"
#include "fs/vfs_syscall.h"
#include "fs/fcntl.h"
#include "fs/stat.h"

#include "test/kshell/kshell.h"
#include "test/s5fs_test.h"

extern void *sunghan_test(int, void*);
extern void *sunghan_deadlock_test(int, void*);
extern void *faber_thread_test(int, void*);
extern int vfstest_main(int argc, char **argv);
extern int faber_fs_thread_test(kshell_t *ksh, int argc, char **argv);
extern int faber_directory_test(kshell_t *ksh, int argc, char **argv);

GDB_DEFINE_HOOK(boot)
GDB_DEFINE_HOOK(initialized)
GDB_DEFINE_HOOK(shutdown)

static void      *bootstrap(int arg1, void *arg2);
static void      *idleproc_run(int arg1, void *arg2);
static kthread_t *initproc_create(void);
static void      *initproc_run(int arg1, void *arg2);
static void       hard_shutdown(void);

static context_t bootstrap_context;
extern int gdb_wait;

/**
 * This is the first real C function ever called. It performs a lot of
 * hardware-specific initialization, then creates a pseudo-context to
 * execute the bootstrap function in.
 */
void
kmain()
{
        GDB_CALL_HOOK(boot);

        dbg_init();
        dbgq(DBG_CORE, "Kernel binary:\n");
        dbgq(DBG_CORE, "  text: 0x%p-0x%p\n", &kernel_start_text, &kernel_end_text);
        dbgq(DBG_CORE, "  data: 0x%p-0x%p\n", &kernel_start_data, &kernel_end_data);
        dbgq(DBG_CORE, "  bss:  0x%p-0x%p\n", &kernel_start_bss, &kernel_end_bss);

        page_init();

        pt_init();
        slab_init();
        pframe_init();

        acpi_init();
        apic_init();
        pci_init();
        intr_init();

        gdt_init();

        /* initialize slab allocators */
#ifdef __VM__
        anon_init();
        shadow_init();
#endif
        vmmap_init();
        proc_init();
        kthread_init();

#ifdef __DRIVERS__
        bytedev_init();
        blockdev_init();
#endif

        void *bstack = page_alloc();
        pagedir_t *bpdir = pt_get();
        KASSERT(NULL != bstack && "Ran out of memory while booting.");
        /* This little loop gives gdb a place to synch up with weenix.  In the
         * past the weenix command started qemu was started with -S which
         * allowed gdb to connect and start before the boot loader ran, but
         * since then a bug has appeared where breakpoints fail if gdb connects
         * before the boot loader runs.  See
         *
         * https://bugs.launchpad.net/qemu/+bug/526653
         *
         * This loop (along with an additional command in init.gdb setting
         * gdb_wait to 0) sticks weenix at a known place so gdb can join a
         * running weenix, set gdb_wait to zero  and catch the breakpoint in
         * bootstrap below.  See Config.mk for how to set GDBWAIT correctly.
         *
         * DANGER: if GDBWAIT != 0, and gdb is not running, this loop will never
         * exit and weenix will not run.  Make SURE the GDBWAIT is set the way
         * you expect.
         */
        while (gdb_wait) ;
        context_setup(&bootstrap_context, bootstrap, 0, NULL, bstack, PAGE_SIZE, bpdir);
        context_make_active(&bootstrap_context);

        panic("\nReturned to kmain()!!!\n");
}

/**
 * Clears all interrupts and halts, meaning that we will never run
 * again.
 */
static void
hard_shutdown()
{
#ifdef __DRIVERS__
        vt_print_shutdown();
#endif
        __asm__ volatile("cli; hlt");
}

/**
 * This function is called from kmain, however it is not running in a
 * thread context yet. It should create the idle process which will
 * start executing idleproc_run() in a real thread context.  To start
 * executing in the new process's context call context_make_active(),
 * passing in the appropriate context. This function should _NOT_
 * return.
 *
 * Note: Don't forget to set curproc and curthr appropriately.
 *
 * @param arg1 the first argument (unused)
 * @param arg2 the second argument (unused)
 */
static void *
bootstrap(int arg1, void *arg2)
{
        /* If the next line is removed/altered in your submission, 20 points will be deducted. */
        dbgq(DBG_TEST, "SIGNATURE: 53616c7465645f5f7f318b1bc2d4083954d94bef93a84e7c1f7447503848ed6371756b3aea509eaadb3d75eefe1c8622\n");
        /* necessary to finalize page table information */
        pt_template_init();

//        NOT_YET_IMPLEMENTED("PROCS: bootstrap");
        char *name = "idle process";
        proc_t *p = proc_create(name);
        kthread_t *t = kthread_create(p, idleproc_run, 0, NULL);
        curproc = p;
        curthr = t;
        t->kt_state = KT_RUN;
        KASSERT(NULL != curproc); /* curproc was uninitialized before, it is initialized here to point to the "idle" process */
        KASSERT(PID_IDLE == curproc->p_pid); /* make sure the process ID of the created "idle" process is PID_IDLE */
        KASSERT(NULL != curthr); /* curthr was uninitialized before, it is initialized here to point to the thread of the "idle" process */
        dbg(DBG_PRINT, "(GRADING1A 1.a)\n");
        dbg(DBG_PRINT, "(GRADING1A)\n");
        context_make_active(&(t->kt_ctx));

        panic("weenix returned to bootstrap()!!! BAD!!!\n");
        return NULL;
}

/**
 * Once we're inside of idleproc_run(), we are executing in the context of the
 * first process-- a real context, so we can finally begin running
 * meaningful code.
 *
 * This is the body of process 0. It should initialize all that we didn't
 * already initialize in kmain(), launch the init process (initproc_run),
 * wait for the init process to exit, then halt the machine.
 *
 * @param arg1 the first argument (unused)
 * @param arg2 the second argument (unused)
 */
static void *
idleproc_run(int arg1, void *arg2)
{
        int status;
        pid_t child;

        /* create init proc */
        kthread_t *initthr = initproc_create();
        init_call_all();
        GDB_CALL_HOOK(initialized);

        /* Create other kernel threads (in order) */
    //for test
    struct stat test_s;

#ifdef __VFS__
        /* Once you have VFS remember to set the current working directory
         * of the idle and init processes */
        curproc->p_cwd = vfs_root_vn;
        vref(vfs_root_vn);
        initthr->kt_proc->p_cwd = vfs_root_vn;
        vref(vfs_root_vn);
        //NOT_YET_IMPLEMENTED("VFS: idleproc_run");


        /* Here you need to make the null, zero, and tty devices using mknod */
        /* You can't do this until you have VFS, check the include/drivers/dev.h
         * file for macros with the device ID's you will need to pass to mknod */
        do_mkdir("/dev");
        do_mknod("/dev/null", S_IFCHR, MKDEVID(1,0));
        do_mknod("/dev/zero", S_IFCHR, MKDEVID(1,1));
        do_mknod("/dev/tty0", S_IFCHR, MKDEVID(2,0));
        dbg(DBG_PRINT, "(GRADING2B)\n");
//        NOT_YET_IMPLEMENTED("VFS: idleproc_run");
#endif

        /* Finally, enable interrupts (we want to make sure interrupts
         * are enabled AFTER all drivers are initialized) */
        intr_enable();

        /* Run initproc */
        sched_make_runnable(initthr);
        /* Now wait for it */
        child = do_waitpid(-1, 0, &status);
        KASSERT(PID_INIT == child);

#ifdef __MTP__
        kthread_reapd_shutdown();
#endif


#ifdef __SHADOWD__
        /* wait for shadowd to shutdown */
        shadowd_shutdown();
#endif

#ifdef __VFS__
        /* Shutdown the vfs: */
        dbg_print("weenix: vfs shutdown...\n");
        vput(curproc->p_cwd);
        if (vfs_shutdown())
                panic("vfs shutdown FAILED!!\n");

#endif

        /* Shutdown the pframe system */
#ifdef __S5FS__
        pframe_shutdown();
#endif

        dbg_print("\nweenix: halted cleanly!\n");
        GDB_CALL_HOOK(shutdown);
        hard_shutdown();
        return NULL;
}

/**
 * This function, called by the idle process (within 'idleproc_run'), creates the
 * process commonly refered to as the "init" process, which should have PID 1.
 *
 * The init process should contain a thread which begins execution in
 * initproc_run().
 *
 * @return a pointer to a newly created thread which will execute
 * initproc_run when it begins executing
 */
static kthread_t *
initproc_create(void)
{
//        NOT_YET_IMPLEMENTED("PROCS: initproc_create");
        char *name = "init process";
        proc_t *p = proc_create(name);
        kthread_t *t = kthread_create(p, initproc_run, 0, NULL);
        KASSERT(NULL != p);
        KASSERT(PID_INIT == p->p_pid);
        KASSERT(NULL != t);
        dbg(DBG_PRINT, "(GRADING1A 1.b)\n");
        dbg(DBG_PRINT, "(GRADING1A)\n");
        return t;
}

#ifdef __DRIVERS__
int do_foo(kshell_t *kshell, int argc, char **argv)
{
    KASSERT(kshell != NULL);
    dbg(DBG_TEMP, "(GRADING#X Y.Z): do_foo() is invoked, argc = %d, argv = 0x%08x\n",
        argc, (unsigned int)argv);
    /*
     * Must not call a test function directly.
     * Must create a child process, create a thread in that process and
     *     set the test function as the first procedure of that thread,
     *     then wait for the child process to die.
     */
    return 0;
}

int my_faber_thread_test(kshell_t *kshell, int argc, char **argv) {
    KASSERT(kshell != NULL);

    proc_t *p = proc_create("my_faber_test");
    kthread_t *t = kthread_create(p, faber_thread_test, 0, NULL);
    sched_make_runnable(t);
    int status;
    do_waitpid(p->p_pid, 0, &status);

    return 0;
}

int my_sunghan_test(kshell_t *kshell, int argc, char **argv) {
    KASSERT(kshell != NULL);

    proc_t *p = proc_create("my_sunghan_test");
    kthread_t *t = kthread_create(p, sunghan_test, 0, NULL);
    sched_make_runnable(t);
    int status;
    do_waitpid(p->p_pid, 0, &status);

    return 0;
}

int my_sunghan_deadlock_test(kshell_t *kshell, int argc, char **argv) {
    KASSERT(kshell != NULL);

    proc_t *p = proc_create("my_sunghan_deadlock_test");
    kthread_t *t = kthread_create(p, sunghan_deadlock_test, 0, NULL);
    sched_make_runnable(t);
    int status;
    do_waitpid(p->p_pid, 0, &status);

    return 0;
}

void *test_proc_function_cancel_myself(int arg1, void *arg2) {
    kthread_cancel(curthr, 0);
    return NULL;
}

void *test_proc_function_cancel_running_thread(int arg1, void *arg2) {
    return NULL;
}

void *test_proc_function_mutex_sleep_cancel(int arg1, void *arg2) {
    kmutex_t *m_ptr = (kmutex_t *)(arg2);
    if (kmutex_lock_cancellable(m_ptr) == -EINTR) {
        dbg(DBG_PRINT, "(GRADING1C)\n");
        return NULL;
    }
    kmutex_unlock(m_ptr);
    dbg(DBG_PRINT, "(GRADING1C)\n");
    return NULL;
}

void *test(int arg1, void *arg2) {
    proc_t *p = proc_create("test_cancel_myself");
    kthread_t *t = kthread_create(p, test_proc_function_cancel_myself, 0, NULL);
    sched_make_runnable(t);
    int status;
    do_waitpid(-1, 0, &status);

    p = proc_create("test_cancel_a_running_thread");
    t = kthread_create(p, test_proc_function_cancel_running_thread, 0, NULL);
    sched_make_runnable(t);
    kthread_cancel(t, &status);
    do_waitpid(-1, 0, &status);

    kmutex_t m;
    kmutex_init(&m);
    kmutex_lock(&m);
    p = proc_create("test_mutex_sleep_cancel");
    t = kthread_create(p, test_proc_function_mutex_sleep_cancel, 0, (void *)(&m));
    sched_make_runnable(t);
    sched_make_runnable(curthr);
    sched_switch();
    kthread_cancel(t, &status);
//    kmutex_unlock(&m);
    do_waitpid(-1, 0, &status);
    dbg(DBG_PRINT, "(GRADING1C)\n");
    kmutex_unlock(&m);
    return NULL;
}

int my_test(kshell_t *kshell, int argc, char **argv) {
    KASSERT(kshell != NULL);

    proc_t *p = proc_create("my_test");
    kthread_t *t = kthread_create(p, test, 0, NULL);
    sched_make_runnable(t);
    int status;
    do_waitpid(p->p_pid, 0, &status);

    return 0;
}


int my_vfstest(kshell_t *kshell, int argc, char **argv) {
    KASSERT(kshell != NULL);

    proc_t *p = proc_create("my_vfstest");
    kthread_t *t = kthread_create(p, (void *)vfstest_main, 1, NULL);
    sched_make_runnable(t);
    int status;
    do_waitpid(p->p_pid, 0, &status);

    return 0;
}

#endif /* __DRIVERS__ */



/**
 * The init thread's function changes depending on how far along your Weenix is
 * developed. Before VM/FI, you'll probably just want to have this run whatever
 * tests you've written (possibly in a new process). After VM/FI, you'll just
 * exec "/sbin/init".
 *
 * Both arguments are unused.
 *
 * @param arg1 the first argument (unused)
 * @param arg2 the second argument (unused)
 */
static void *
initproc_run(int arg1, void *arg2)
{
        // NOT_YET_IMPLEMENTED("PROCS: initproc_run");
//        faber_thread_test(0, NULL);
//         char *empty_args[2] = {"hello", NULL};
//         char *empty_envp[1] = {NULL};
//         kernel_execve("/usr/bin/hello", empty_args, empty_envp);
        // char *empty_args[] = {"/usr/bin/args", "ab", "cde", "fghi", "j", NULL};
        // char *empty_envp[1] = {NULL};
        // kernel_execve("/usr/bin/args", empty_args, empty_envp);
        // char *empty_args[] = {"/bin/uname", "-a", NULL};
        // char *empty_envp[1] = {NULL};
        // kernel_execve("/bin/uname", empty_args, empty_envp);
        // char *empty_args[] = {"/bin/stat", "/README", NULL};
        // char *empty_envp[1] = {NULL};
        // kernel_execve("/bin/stat", empty_args, empty_envp);
        // char *empty_args[] = {"/bin/stat", "/usr", NULL};
        // char *empty_envp[1] = {NULL};
        // kernel_execve("/bin/stat", empty_args, empty_envp);
//        char *empty_args[] = {"/bin/ls", "/usr/bin", NULL};
//        char *empty_envp[1] = {NULL};
//        kernel_execve("/bin/ls", empty_args, empty_envp);

//         char *empty_args[] = {"/usr/bin/fork-and-wait", NULL};
//         char *empty_envp[1] = {NULL};
//         kernel_execve("/usr/bin/fork-and-wait", empty_args, empty_envp);

        char *argv[] = { "/sbin/init", NULL };
        char *envp[] = { NULL };
        kernel_execve("/sbin/init", argv, envp);
        
#ifdef __DRIVERS__
        kshell_add_command("sunghan", my_sunghan_test, "Run sunghan_test().");
        kshell_add_command("deadlock", my_sunghan_deadlock_test, "Run sunghan_deadlock_test().");
        kshell_add_command("faber", my_faber_thread_test, "Run faber_thread_test().");
        kshell_add_command("foo", do_foo, "invoke do_foo() to print a message...");
        kshell_add_command("test", my_test, "invoke test...");
        kshell_add_command("vfstest", my_vfstest, "call vfstest...");
        kshell_add_command("faber_fs_thread_test", faber_fs_thread_test, "call faber_fs_thread_test...");
        kshell_add_command("faber_directory_test", faber_directory_test, "call faber_directory_test...");
        kshell_t *kshell = kshell_create(0);
        if (NULL == kshell) panic("init: Couldn't create kernel shell\n");
        while (kshell_execute_next(kshell));
        kshell_destroy(kshell);
#endif /* __DRIVERS__ */

        while(!list_empty(&curproc->p_children)) {
            int status;
            do_waitpid(-1, 0, &status);
            dbg(DBG_PRINT, "(GRADING1A)\n");
        }

        dbg(DBG_PRINT, "(GRADING1A)\n");
        return NULL;
}
