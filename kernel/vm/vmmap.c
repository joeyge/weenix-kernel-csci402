/******************************************************************************/
/* Important Spring 2022 CSCI 402 usage information:                          */
/*                                                                            */
/* This fils is part of CSCI 402 kernel programming assignments at USC.       */
/*         53616c7465645f5fd1e93dbf35cbffa3aef28f8c01d8cf2ffc51ef62b26a       */
/*         f9bda5a68e5ed8c972b17bab0f42e24b19daa7bd408305b1f7bd6c7208c1       */
/*         0e36230e913039b3046dd5fd0ba706a624d33dbaa4d6aab02c82fe09f561       */
/*         01b0fd977b0051f0b0ce0c69f7db857b1b5e007be2db6d42894bf93de848       */
/*         806d9152bd5715e9                                                   */
/* Please understand that you are NOT permitted to distribute or publically   */
/*         display a copy of this file (or ANY PART of it) for any reason.    */
/* If anyone (including your prospective employer) asks you to post the code, */
/*         you must inform them that you do NOT have permissions to do so.    */
/* You are also NOT permitted to remove or alter this comment block.          */
/* If this comment block is removed or altered in a submitted file, 20 points */
/*         will be deducted.                                                  */
/******************************************************************************/

#include "kernel.h"
#include "errno.h"
#include "globals.h"

#include "vm/vmmap.h"
#include "vm/shadow.h"
#include "vm/anon.h"

#include "proc/proc.h"

#include "util/debug.h"
#include "util/list.h"
#include "util/string.h"
#include "util/printf.h"

#include "fs/vnode.h"
#include "fs/file.h"
#include "fs/fcntl.h"
#include "fs/vfs_syscall.h"

#include "mm/slab.h"
#include "mm/page.h"
#include "mm/mm.h"
#include "mm/mman.h"
#include "mm/mmobj.h"

static slab_allocator_t *vmmap_allocator;
static slab_allocator_t *vmarea_allocator;

void
vmmap_init(void)
{
        vmmap_allocator = slab_allocator_create("vmmap", sizeof(vmmap_t));
        KASSERT(NULL != vmmap_allocator && "failed to create vmmap allocator!");
        vmarea_allocator = slab_allocator_create("vmarea", sizeof(vmarea_t));
        KASSERT(NULL != vmarea_allocator && "failed to create vmarea allocator!");
}

vmarea_t *
vmarea_alloc(void)
{
        vmarea_t *newvma = (vmarea_t *) slab_obj_alloc(vmarea_allocator);
        if (newvma) {
                newvma->vma_vmmap = NULL;
        }
        return newvma;
}

void
vmarea_free(vmarea_t *vma)
{
        KASSERT(NULL != vma);
        slab_obj_free(vmarea_allocator, vma);
}

/* a debugging routine: dumps the mappings of the given address space. */
size_t
vmmap_mapping_info(const void *vmmap, char *buf, size_t osize)
{
        KASSERT(0 < osize);
        KASSERT(NULL != buf);
        KASSERT(NULL != vmmap);

        vmmap_t *map = (vmmap_t *)vmmap;
        vmarea_t *vma;
        ssize_t size = (ssize_t)osize;

        int len = snprintf(buf, size, "%21s %5s %7s %8s %10s %12s\n",
                           "VADDR RANGE", "PROT", "FLAGS", "MMOBJ", "OFFSET",
                           "VFN RANGE");

        list_iterate_begin(&map->vmm_list, vma, vmarea_t, vma_plink) {
                size -= len;
                buf += len;
                if (0 >= size) {
                        goto end;
                }

                len = snprintf(buf, size,
                               "%#.8x-%#.8x  %c%c%c  %7s 0x%p %#.5x %#.5x-%#.5x\n",
                               vma->vma_start << PAGE_SHIFT,
                               vma->vma_end << PAGE_SHIFT,
                               (vma->vma_prot & PROT_READ ? 'r' : '-'),
                               (vma->vma_prot & PROT_WRITE ? 'w' : '-'),
                               (vma->vma_prot & PROT_EXEC ? 'x' : '-'),
                               (vma->vma_flags & MAP_SHARED ? " SHARED" : "PRIVATE"),
                               vma->vma_obj, vma->vma_off, vma->vma_start, vma->vma_end);
        } list_iterate_end();

end:
        if (size <= 0) {
                size = osize;
                buf[osize - 1] = '\0';
        }
        /*
        KASSERT(0 <= size);
        if (0 == size) {
                size++;
                buf--;
                buf[0] = '\0';
        }
        */
        return osize - size;
}

/* Create a new vmmap, which has no vmareas and does
 * not refer to a process. */
vmmap_t *
vmmap_create(void)
{
//        NOT_YET_IMPLEMENTED("VM: vmmap_create");
        vmmap_t * new_vmmap = (vmmap_t *)slab_obj_alloc(vmmap_allocator);
        if (new_vmmap) {
            list_init(&(new_vmmap->vmm_list));
            new_vmmap->vmm_proc = NULL;
            dbg(DBG_PRINT, "(GRADING3A)\n");
        }
        dbg(DBG_PRINT, "(GRADING3A)\n");
        return new_vmmap;
}

/* Removes all vmareas from the address space and frees the
 * vmmap struct. */
void
vmmap_destroy(vmmap_t *map)
{
//        NOT_YET_IMPLEMENTED("VM: vmmap_destroy");
        KASSERT(NULL != map);
        dbg(DBG_PRINT, "(GRADING3A 3.a)\n");

        vmarea_t* vmarea = NULL;
        list_iterate_begin(&(map->vmm_list), vmarea, vmarea_t, vma_plink) {
                    if (list_link_is_linked(&vmarea->vma_olink)) {
                            list_remove(&(vmarea->vma_olink));
                            dbg(DBG_PRINT, "(GRADING3A)\n");
                    }
                    list_remove(&(vmarea->vma_plink));
                    vmarea->vma_obj->mmo_ops->put(vmarea->vma_obj);
                    vmarea_free(vmarea);
                    dbg(DBG_PRINT, "(GRADING3A)\n");
        } list_iterate_end();
        map->vmm_proc = NULL;
        slab_obj_free(vmmap_allocator, map);
        dbg(DBG_PRINT, "(GRADING3A)\n");
}

/* Add a vmarea to an address space. Assumes (i.e. asserts to some extent)
 * the vmarea is valid.  This involves finding where to put it in the list
 * of VM areas, and adding it. Don't forget to set the vma_vmmap for the
 * area. */
void
vmmap_insert(vmmap_t *map, vmarea_t *newvma)
{
//        NOT_YET_IMPLEMENTED("VM: vmmap_insert");
        KASSERT(NULL != map && NULL != newvma);
        dbg(DBG_PRINT, "(GRADING3A 3.b)\n");
        KASSERT(NULL == newvma->vma_vmmap);
        dbg(DBG_PRINT, "(GRADING3A 3.b)\n");
        KASSERT(newvma->vma_start < newvma->vma_end);
        dbg(DBG_PRINT, "(GRADING3A 3.b)\n");
        KASSERT(ADDR_TO_PN(USER_MEM_LOW) <= newvma->vma_start && ADDR_TO_PN(USER_MEM_HIGH) >= newvma->vma_end);
        dbg(DBG_PRINT, "(GRADING3A 3.b)\n");

        newvma->vma_vmmap = map;
        vmarea_t* vmarea = NULL;
        list_iterate_begin(&(map->vmm_list), vmarea, vmarea_t, vma_plink) {
            if (vmarea->vma_start > newvma->vma_start) {
                list_insert_before(&(vmarea->vma_plink), &(newvma->vma_plink));
                dbg(DBG_PRINT, "(GRADING3A)\n");
                return;
            }
        } list_iterate_end();
        list_insert_before(&(map->vmm_list), &(newvma->vma_plink));
        dbg(DBG_PRINT, "(GRADING3A)\n");

}

/* Find a contiguous range of free virtual pages of length npages in
 * the given address space. Returns starting vfn for the range,
 * without altering the map. Returns -1 if no such range exists.
 *
 * Your algorithm should be first fit. If dir is VMMAP_DIR_HILO, you
 * should find a gap as high in the address space as possible; if dir
 * is VMMAP_DIR_LOHI, the gap should be as low as possible. */
int
vmmap_find_range(vmmap_t *map, uint32_t npages, int dir)
{
//        NOT_YET_IMPLEMENTED("VM: vmmap_find_range");
        int ret = -1;
        vmarea_t *vmarea = NULL;
        vmarea_t *tmp = NULL;
        if (dir == VMMAP_DIR_HILO) {
            list_iterate_reverse(&(map->vmm_list), vmarea, vmarea_t, vma_plink) {
                if (!tmp) {
                    if (ADDR_TO_PN(USER_MEM_HIGH) - vmarea->vma_end >= npages) {
                        ret = ADDR_TO_PN(USER_MEM_HIGH) - npages;
                        dbg(DBG_PRINT, "(GRADING3A)\n");
                        break;
                    }
                } else {
                    if (tmp->vma_start - vmarea->vma_end >= npages) {
                        ret = tmp->vma_start - npages;
                        dbg(DBG_PRINT, "(GRADING3D 1)\n");
                        break;
                    }
                }
                tmp = vmarea;
                dbg(DBG_PRINT, "(GRADING3D 1)\n");
            } list_iterate_end();
            if (ret == -1) {
                if (tmp->vma_start - ADDR_TO_PN(USER_MEM_LOW) >= npages) {
                    ret = tmp->vma_start - npages;
                    dbg(DBG_PRINT, "(GRADING3D 2)\n");
                }
            }
        } 
        // else {
        //     list_iterate_begin(&(map->vmm_list), vmarea, vmarea_t, vma_plink) {
        //         if (!tmp) {
        //             if (vmarea->vma_start - ADDR_TO_PN(USER_MEM_LOW) >= npages) {
        //                 ret = ADDR_TO_PN(USER_MEM_LOW);
        //                 dbg(DBG_PRINT, "(GRADING3A TODO)\n");
        //                 break;
        //             }
        //         } else {
        //             if (vmarea->vma_start - tmp->vma_end >= npages) {
        //                 ret = tmp->vma_end;
        //                 dbg(DBG_PRINT, "(GRADING3A TODO)\n");
        //                 break;
        //             }
        //         }
        //         tmp = vmarea;
        //         dbg(DBG_PRINT, "(GRADING3A TODO)\n");
        //     } list_iterate_end();
        //     if (ret == -1) {
        //         if (ADDR_TO_PN(USER_MEM_HIGH) - tmp->vma_end >= npages) {
        //             ret = tmp->vma_end;
        //             dbg(DBG_PRINT, "(GRADING3A TODO)\n");
        //         }
        //     }
        // }
        dbg(DBG_PRINT, "(GRADING3A)\n");
        return ret;
}

/* Find the vm_area that vfn lies in. Simply scan the address space
 * looking for a vma whose range covers vfn. If the page is unmapped,
 * return NULL. */
vmarea_t *
vmmap_lookup(vmmap_t *map, uint32_t vfn)
{
//        NOT_YET_IMPLEMENTED("VM: vmmap_lookup");
        KASSERT(NULL != map);
        dbg(DBG_PRINT, "(GRADING3A 3.c)\n");

        vmarea_t *vmarea = NULL;
        list_iterate_begin(&(map->vmm_list), vmarea, vmarea_t, vma_plink) {
            if (vfn >= vmarea->vma_start && vfn < vmarea->vma_end) {
                dbg(DBG_PRINT, "(GRADING3A)\n");
                return vmarea;
            }
        } list_iterate_end();
        dbg(DBG_PRINT, "(GRADING3A)\n");
        return NULL;
}

/* Allocates a new vmmap containing a new vmarea for each area in the
 * given map. The areas should have no mmobjs set yet. Returns pointer
 * to the new vmmap on success, NULL on failure. This function is
 * called when implementing fork(2). */
vmmap_t *
vmmap_clone(vmmap_t *map)
{
//        NOT_YET_IMPLEMENTED("VM: vmmap_clone");
        vmmap_t *new_vmmap = vmmap_create();
        vmarea_t *vmarea = NULL;
        list_iterate_begin(&(map->vmm_list), vmarea, vmarea_t, vma_plink) {
            vmarea_t *vma = vmarea_alloc();
            vma->vma_start = vmarea->vma_start;
            vma->vma_end = vmarea->vma_end;
            vma->vma_off = vmarea->vma_off;
            vma->vma_prot = vmarea->vma_prot;
            vma->vma_flags = vmarea->vma_flags;
            vma->vma_vmmap = new_vmmap;
            list_link_init(&(vma->vma_plink));
            list_link_init(&(vma->vma_olink));
            list_insert_tail(&(new_vmmap->vmm_list), &(vma->vma_plink));
            dbg(DBG_PRINT, "(GRADING3A)\n");
        } list_iterate_end();
        dbg(DBG_PRINT, "(GRADING3A)\n");
        return new_vmmap;
}

/* Insert a mapping into the map starting at lopage for npages pages.
 * If lopage is zero, we will find a range of virtual addresses in the
 * process that is big enough, by using vmmap_find_range with the same
 * dir argument.  If lopage is non-zero and the specified region
 * contains another mapping that mapping should be unmapped.
 *
 * If file is NULL an anon mmobj will be used to create a mapping
 * of 0's.  If file is non-null that vnode's file will be mapped in
 * for the given range.  Use the vnode's mmap operation to get the
 * mmobj for the file; do not assume it is file->vn_obj. Make sure all
 * of the area's fields except for vma_obj have been set before
 * calling mmap.
 *
 * If MAP_PRIVATE is specified set up a shadow object for the mmobj.
 *
 * All of the input to this function should be valid (KASSERT!).
 * See mmap(2) for for description of legal input.
 * Note that off should be page aligned.
 *
 * Be very careful about the order operations are performed in here. Some
 * operation are impossible to undo and should be saved until there
 * is no chance of failure.
 *
 * If 'new' is non-NULL a pointer to the new vmarea_t should be stored in it.
 */
int
vmmap_map(vmmap_t *map, vnode_t *file, uint32_t lopage, uint32_t npages,
          int prot, int flags, off_t off, int dir, vmarea_t **new)
{
        // NOT_YET_IMPLEMENTED("VM: vmmap_map");
        // return -1;

        KASSERT(NULL != map); /* must not add a memory segment into a non-existing vmmap */
        dbg(DBG_PRINT, "(GRADING3A 3.d)\n");
        KASSERT(0 < npages); /* number of pages of this memory segment cannot be 0 */
        dbg(DBG_PRINT, "(GRADING3A 3.d)\n");
        KASSERT((MAP_SHARED & flags) || (MAP_PRIVATE & flags)); /* must specify whether the memory segment is shared or private */
        dbg(DBG_PRINT, "(GRADING3A 3.d)\n");
        KASSERT((0 == lopage) || (ADDR_TO_PN(USER_MEM_LOW) <= lopage)); /* if lopage is not zero, it must be a user space vpn */
        dbg(DBG_PRINT, "(GRADING3A 3.d)\n");
        /* if lopage is not zero, the specified page range must lie completely within the user space */
        KASSERT((0 == lopage) || (ADDR_TO_PN(USER_MEM_HIGH) >= (lopage + npages)));
        dbg(DBG_PRINT, "(GRADING3A 3.d)\n");
        KASSERT(PAGE_ALIGNED(off)); /* the off argument must be page aligned */
        dbg(DBG_PRINT, "(GRADING3A 3.d)\n");

        int start_vfn;
        if (lopage == 0) { // find a range of virtual addresses in the process
                if ((start_vfn = vmmap_find_range(map, npages, dir)) == -1) {
                        dbg(DBG_PRINT, "(GRADING3D 2)\n");
                        return start_vfn;
                }
                dbg(DBG_PRINT, "(GRADING3A)\n");
        } else {
                if (vmmap_is_range_empty(map, lopage, npages) == 0) {   // the specified region contains another mapping
                        vmmap_remove(map, lopage, npages);
                        dbg(DBG_PRINT, "(GRADING3A)\n");
                }
                start_vfn = lopage;
                dbg(DBG_PRINT, "(GRADING3A)\n");
        }

        vmarea_t *vmarea = vmarea_alloc();
        if (vmarea == NULL) {
                dbg(DBG_PRINT, "(GRADING3D 4)\n");
                return -ENOMEM;
        }
        vmarea->vma_start = (uint32_t) start_vfn;        
        vmarea->vma_end = vmarea->vma_start + npages;
        vmarea->vma_off = ADDR_TO_PN(off);
        vmarea->vma_prot = prot;
        vmarea->vma_flags = flags;
        list_link_init(&vmarea->vma_plink);
        list_link_init(&vmarea->vma_olink);

        mmobj_t *obj;
        if (file == NULL) { // an anon mmobj will be used to create a mapping of 0's
                obj = anon_create();
                dbg(DBG_PRINT, "(GRADING3A)\n");
        } else { // the vnode's file will be mapped in for the given range
                file->vn_ops->mmap(file, vmarea, &obj);
                dbg(DBG_PRINT, "(GRADING3A)\n");
        }

        if ((flags & MAP_PRIVATE) == MAP_PRIVATE) { // set up a shadow object for the mmobj
                mmobj_t *shadow = shadow_create();
                shadow->mmo_shadowed = obj;
                shadow->mmo_un.mmo_bottom_obj = mmobj_bottom_obj(obj);
                mmobj_t *btm_obj = shadow->mmo_un.mmo_bottom_obj;
                btm_obj->mmo_ops->ref(btm_obj);
                list_insert_head(&btm_obj->mmo_un.mmo_vmas, &vmarea->vma_olink);                
                obj = shadow;
                dbg(DBG_PRINT, "(GRADING3A)\n");
        } else {
                list_insert_head(&obj->mmo_un.mmo_vmas, &vmarea->vma_olink);
                dbg(DBG_PRINT, "(GRADING3D 1)\n");
        }
        vmarea->vma_obj = obj;
        vmmap_insert(map, vmarea); // add the vmarea to the map's address space
        if (new != NULL) { // a pointer to the new vmarea_t should be stored in new
                *new = vmarea;
                dbg(DBG_PRINT, "(GRADING3A)\n");
        }
        dbg(DBG_PRINT, "(GRADING3A)\n");

        return 0;
}

/*
 * We have no guarantee that the region of the address space being
 * unmapped will play nicely with our list of vmareas.
 *
 * You must iterate over each vmarea that is partially or wholly covered
 * by the address range [addr ... addr+len). The vm-area will fall into one
 * of four cases, as illustrated below:
 *
 * key:
 *          [             ]   Existing VM Area
 *        *******             Region to be unmapped
 *
 * Case 1:  [   ******    ]
 * The region to be unmapped lies completely inside the vmarea. We need to
 * split the old vmarea into two vmareas. be sure to increment the
 * reference count to the file associated with the vmarea.
 *
 * Case 2:  [      *******]**
 * The region overlaps the end of the vmarea. Just shorten the length of
 * the mapping.
 *
 * Case 3: *[*****        ]
 * The region overlaps the beginning of the vmarea. Move the beginning of
 * the mapping (remember to update vma_off), and shorten its length.
 *
 * Case 4: *[*************]**
 * The region completely contains the vmarea. Remove the vmarea from the
 * list.
 */
int
vmmap_remove(vmmap_t *map, uint32_t lopage, uint32_t npages)
{
        // NOT_YET_IMPLEMENTED("VM: vmmap_remove");
        // return -1;

        uint32_t hipage = lopage + npages;
        vmarea_t *vmarea;
        list_iterate_begin(&map->vmm_list, vmarea, vmarea_t, vma_plink) {
                if (vmarea->vma_start < lopage && hipage < vmarea->vma_end) { // Case 1:  [   ******    ]
                        vmarea_t *split_vmarea = vmarea_alloc();
                        split_vmarea->vma_start = hipage;
                        split_vmarea->vma_end = vmarea->vma_end;
                        split_vmarea->vma_off = vmarea->vma_off + hipage - vmarea->vma_start; 
                        split_vmarea->vma_prot = vmarea->vma_prot;
                        split_vmarea->vma_flags = vmarea->vma_flags;
                        split_vmarea->vma_obj = vmarea->vma_obj;
                        vmarea->vma_end = lopage;
                        if (split_vmarea->vma_obj != NULL) { // if?
                                split_vmarea->vma_obj->mmo_ops->ref(split_vmarea->vma_obj);
                                dbg(DBG_PRINT, "(GRADING3D 2)\n");
                        }
                        list_link_init(&split_vmarea->vma_plink);
                        list_link_init(&split_vmarea->vma_olink);
                        list_insert_tail(&vmarea->vma_olink, &split_vmarea->vma_olink);
                        vmmap_insert(map, split_vmarea);
                        dbg(DBG_PRINT, "(GRADING3D 2)\n");
                } else if (vmarea->vma_start < lopage && lopage < vmarea->vma_end && vmarea->vma_end <= hipage) { // Case 2:  [      *******]** 
                        vmarea->vma_end = lopage;
                        dbg(DBG_PRINT, "(GRADING3D 1)\n");
                } else if (lopage <= vmarea->vma_start && vmarea->vma_start < hipage && hipage < vmarea->vma_end) { // Case 3: *[*****        ]
                        vmarea->vma_off += hipage - vmarea->vma_start;
                        vmarea->vma_start = hipage;
                        dbg(DBG_PRINT, "(GRADING3D 2)\n");
                } else if (vmarea->vma_start >= lopage && vmarea->vma_end <= hipage) { // Case 4: *[*************]**
                        vmarea->vma_obj->mmo_ops->put(vmarea->vma_obj);
                        list_remove(&vmarea->vma_plink);
                        if (list_link_is_linked(&vmarea->vma_olink)) {
                                list_remove(&vmarea->vma_olink);
                                dbg(DBG_PRINT, "(GRADING3A)\n");
                        }
                        vmarea_free(vmarea);
                        dbg(DBG_PRINT, "(GRADING3A)\n");
                } 
        } list_iterate_end();
        pt_unmap_range(curproc->p_pagedir, (uintptr_t)PN_TO_ADDR(lopage), (uintptr_t)PN_TO_ADDR(hipage));
        dbg(DBG_PRINT, "(GRADING3A)\n");
        return 0;
}

/*
 * Returns 1 if the given address space has no mappings for the
 * given range, 0 otherwise.
 */
int
vmmap_is_range_empty(vmmap_t *map, uint32_t startvfn, uint32_t npages)
{
        // NOT_YET_IMPLEMENTED("VM: vmmap_is_range_empty");
        // return 0;

        uint32_t endvfn = startvfn + npages;
        /* the specified page range must not be empty and lie completely within the user space */
        KASSERT((startvfn < endvfn) && (ADDR_TO_PN(USER_MEM_LOW) <= startvfn) && (ADDR_TO_PN(USER_MEM_HIGH) >= endvfn));
        dbg(DBG_PRINT, "(GRADING3A 3.e)\n");

        vmarea_t *vmarea;
        list_iterate_begin(&map->vmm_list, vmarea, vmarea_t, vma_plink) {
                // Case 2:  [      *******]**
                // Case 3: *[*****        ]
                // Case 4: *[*************]**
                if (vmarea->vma_start < endvfn && vmarea->vma_end > startvfn) {
                        dbg(DBG_PRINT, "(GRADING3A)\n");
                        return 0;
                }
        } list_iterate_end();
        dbg(DBG_PRINT, "(GRADING3A)\n");
        return 1;

}

/* Read into 'buf' from the virtual address space of 'map' starting at
 * 'vaddr' for size 'count'. To do so, you will want to find the vmareas
 * to read from, then find the pframes within those vmareas corresponding
 * to the virtual addresses you want to read, and then read from the
 * physical memory that pframe points to. You should not check permissions
 * of the areas. Assume (KASSERT) that all the areas you are accessing exist.
 * Returns 0 on success, -errno on error.
 */
int
vmmap_read(vmmap_t *map, const void *vaddr, void *buf, size_t count) // Q&A: Can you tell me more about "vmmap_read()"?
{
        // NOT_YET_IMPLEMENTED("VM: vmmap_read");
        // return 0;

        uint32_t vfn_start = ADDR_TO_PN(vaddr);
        uint32_t vfn_end = ADDR_TO_PN((size_t)vaddr + count);
        uint32_t pstart_offset = PAGE_OFFSET(vaddr); // start page's offset

        pframe_t *pframe;
        vmarea_t *vmarea;
        uint32_t addr; // copy from this addr for each vfn in the vmareas
        uint32_t nbytes; // number of bytes to copy into buffer

        uint32_t page = vfn_start; // current page
        int ret;
        while (page <= vfn_end) {
                addr = 0;
                nbytes = 0;
                vmarea = vmmap_lookup(map, page);
                ret = pframe_lookup(vmarea->vma_obj, vmarea->vma_off + page - vmarea->vma_start, 0, &pframe);

                if (page == vfn_start) { // copy from start page 
                        nbytes = PAGE_SIZE - pstart_offset;
                        if (page == vfn_end) { // all needs to be copied is from the same page
                                nbytes = (uint32_t)count;
                                dbg(DBG_PRINT, "(GRADING3A)\n");
                        }
                        addr = (uint32_t)pframe->pf_addr + pstart_offset;
                        dbg(DBG_PRINT, "(GRADING3A)\n");
                } else if (page == vfn_end) { // copy from the end page
                        addr = (uint32_t)pframe->pf_addr;
                        nbytes = PAGE_OFFSET((int) vaddr + count);;
                        dbg(DBG_PRINT, "(GRADING3D 1)\n");
                } else { // copy from the page btw (start, end)
                        addr = (uint32_t) pframe->pf_addr;
                        nbytes = PAGE_SIZE;
                        dbg(DBG_PRINT, "(GRADING3D 4)\n");
                }
                memcpy(buf, (void *)addr, nbytes);
                buf = (void *)((uint32_t)buf + nbytes);
                page++;
                dbg(DBG_PRINT, "(GRADING3A)\n");
        }
        dbg(DBG_PRINT, "(GRADING3A)\n");
        return ret;
}

/* Write from 'buf' into the virtual address space of 'map' starting at
 * 'vaddr' for size 'count'. To do this, you will need to find the correct
 * vmareas to write into, then find the correct pframes within those vmareas,
 * and finally write into the physical addresses that those pframes correspond
 * to. You should not check permissions of the areas you use. Assume (KASSERT)
 * that all the areas you are accessing exist. Remember to dirty pages!
 * Returns 0 on success, -errno on error.
 */
int
vmmap_write(vmmap_t *map, void *vaddr, const void *buf, size_t count)
{
        // NOT_YET_IMPLEMENTED("VM: vmmap_write");
        // return 0;

        uint32_t vfn_start = ADDR_TO_PN(vaddr);
        uint32_t vfn_end = ADDR_TO_PN((size_t)vaddr + count);
        uint32_t pstart_offset = PAGE_OFFSET(vaddr); // start page's offset

        pframe_t *pframe;
        vmarea_t *vmarea;
        uint32_t addr; // copy from this addr for each vfn in the vmareas
        uint32_t nbytes; // number of bytes to copy into buffer

        uint32_t page = vfn_start; // current page
        int ret;
        while (page <= vfn_end) {
                addr = 0;
                nbytes = 0;
                vmarea = vmmap_lookup(map, page);
                ret = pframe_lookup(vmarea->vma_obj, vmarea->vma_off + page - vmarea->vma_start, 1, &pframe);

                if (page == vfn_start) { // copy from start page 
                        nbytes = PAGE_SIZE - pstart_offset;
                        if (page == vfn_end) { // all needs to be copied is from the same page
                                nbytes = (uint32_t)count;
                                dbg(DBG_PRINT, "(GRADING3A)\n");
                        }
                        addr = (uint32_t)pframe->pf_addr + pstart_offset;
                        dbg(DBG_PRINT, "(GRADING3A)\n");
                } else if (page == vfn_end) { // copy from the end page
                        addr = (uint32_t)pframe->pf_addr;
                        nbytes = PAGE_OFFSET((int) vaddr + count);;
                        dbg(DBG_PRINT, "(GRADING3A)\n");
                } else { // copy from the page btw (start, end)
                        addr = (uint32_t) pframe->pf_addr;
                        nbytes = PAGE_SIZE;
                        dbg(DBG_PRINT, "(GRADING3D 4)\n");
                }
                memcpy((void *)addr, buf, nbytes);
                buf = (void *)((uint32_t)buf + nbytes);
                page++;
                pframe_dirty(pframe);
                dbg(DBG_PRINT, "(GRADING3A)\n");
        }
        dbg(DBG_PRINT, "(GRADING3A)\n");
        return ret;
}
